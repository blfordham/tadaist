import os
import requests

from tadaist.descriptors import StringDescriptor
from tadaist.descriptors import IntDescriptor
from tadaist.descriptors import BooleanDescriptor
from tadaist.descriptors import RemoteDescriptor
from tadaist.objects.base_object import BaseObject
from tadaist.objects.project import Project

class Tadaist(BaseObject):
  projects = RemoteDescriptor('projects', {}, Project)

  def __init__(self, api_key):
    super().__init__({})
    os.environ['TADAIST_API_KEY'] = api_key

  @property
  def inbox(self):
    proj = None
    for project in self.projects:
      if hasattr(project, 'inbox_project') and project.inbox_project:
        proj = project
        break
    return proj

  def project(self, value):
    proj = None
    for project in self.projects:
      if project.name == value or project.id == value:
        proj = project
        break
    return proj
  
  def new_project(self):
    return Project()

