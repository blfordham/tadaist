from .base_object import BaseObject
from tadaist.descriptors import StringDescriptor
from tadaist.descriptors import IntDescriptor
from tadaist.descriptors import BooleanDescriptor
from tadaist.descriptors import RemoteDescriptor
from tadaist.objects.section import Section
from tadaist.objects.task import Task

class Project(BaseObject):
  PATH = 'projects'

  name = StringDescriptor('name')
  color = IntDescriptor('color')
  parent_id = IntDescriptor('parent_id')
  order = IntDescriptor('order')
  comment_count = IntDescriptor('comment_count')
  shared = BooleanDescriptor('shared')
  favorite = BooleanDescriptor('favorite')
  inbox_project = BooleanDescriptor('inbox_project')
  team_inbox = BooleanDescriptor('team_inbox')
  sync_id = IntDescriptor('sync_id')

  sections = RemoteDescriptor('sections', {'project_id': "%(id)i"}, Section)
  tasks = RemoteDescriptor('tasks', {'project_id': "%(id)i"}, Task)

  def new_task(self):
    return Task({'project_id': self.id})

  def new_section(self):
    return Section({'project_id': self.id})

  def __repr__(self):
    return "Project: %s" % self.name

