import json
import os
import requests
import uuid

from tadaist.descriptors import IntDescriptor

class BaseObject(object):
  PATH = None
  IF_NO_ID = []

  id = IntDescriptor('id')

  def __init__(self, properties={}):
    self._properties = properties
    self._changed = []

    # if ID isn't set, see if we have stuff to always include
    if 'id' not in properties.keys():
      self._changed = self.IF_NO_ID

  def set_properties(self, properties):
    for key in properties.keys():
      self._properties[key] = properties[key]

  def save(self):
    if self.PATH is None:
      raise Exception("Save isn't implemented")
    if not self._changed:
      return

    data = {}
    for key in self._changed:
      data[key] = self._properties[key]
    
    updated = self._post(self._save_path(), data)
    self.set_properties(updated)
    self._changed = []

  def _get(self, path, params={}, obj=None):
    full_url = self._build_url(path)
    json = requests.get(full_url, params=params, headers=self._headers()).json()
    if obj:
      data = []
      for each in json:
        data.append(obj(each))
      return data
    else:
      return json

  def _post(self, path, data):
    full_url = self._build_url(path)
    headers = self._headers(True)
    response = requests.post(full_url, json=data, headers=headers)
    response.raise_for_status()
    if response.text:
      return response.json()
    else:
      return {}

  def _build_url(self, path):
    return "https://api.todoist.com/rest/v1/%s" % path

  def _headers(self, is_post=False):
    headers = {"Authorization": "Bearer %s" % os.environ['TADAIST_API_KEY']}
    if is_post:
      headers["Content-Type"] = "application/json"
      headers["X-Request-Id"] = str(uuid.uuid4())
    return headers
  def _is_readonly(self, name):
    return name == 'id' or 'readonly' in self.PROP_DEFS[name].keys()

  def _save_path(self):
    if self.id:
      return "%s/%i" % (self.PATH, self.id)
    else:
      return self.PATH