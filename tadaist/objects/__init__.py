from tadaist.descriptors import StringDescriptor
from tadaist.descriptors import IntDescriptor
from tadaist.descriptors import BooleanDescriptor
from tadaist.descriptors import RemoteDescriptor
from tadaist.objects.base_object import BaseObject
from tadaist.objects.project import Project
