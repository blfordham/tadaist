from .base_object import BaseObject
from tadaist.descriptors import StringDescriptor
from tadaist.descriptors import IntDescriptor
from tadaist.descriptors import BooleanDescriptor
from tadaist.descriptors import RemoteDescriptor

class Task(BaseObject):
  PATH = 'tasks'
  IF_NO_ID = ['project_id']

  project_id = IntDescriptor('project_id')
  section_id = IntDescriptor('section_id')
  content = StringDescriptor('content')
  completed = BooleanDescriptor('completed')
  parent_id = IntDescriptor('parent_id')
  Deprecated = IntDescriptor('Deprecated')
  order = IntDescriptor('order')
  priority = IntDescriptor('priority')
  due = StringDescriptor('due')
  url = StringDescriptor('url')
  assignee = IntDescriptor('assignee')

  def close(self):
    path = "tasks/%i/close" % self.id
    self._post(path, {})
  
  def reopen(self):
    path = "tasks/%i/repoen" % self.id
    self._post(path, {})

  def __repr__(self):
    return "Task: %s" % self.content