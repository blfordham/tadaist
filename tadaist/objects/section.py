from .base_object import BaseObject

from tadaist.descriptors import StringDescriptor
from tadaist.descriptors import IntDescriptor
from tadaist.descriptors import BooleanDescriptor
from tadaist.descriptors import RemoteDescriptor

class Section(BaseObject):
  PATH = 'sections'
  IF_NO_ID = ['project_id']

  project_id = IntDescriptor('project_id')
  order = IntDescriptor('order')
  name = StringDescriptor('name')

  def __repr__(self):
    return "Section: %s" % self.name
