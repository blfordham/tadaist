class StringDescriptor:
  TYPE = str

  def __init__(self, name):
    self.name = name

  def __get__(self, obj, objType=None):
    if self.name in obj._properties.keys():
      return obj._properties[self.name]
    else:
      return None

  def __set__(self, obj, value):
    obj._properties[self.name] = value
    if self.name not in obj._changed:
      obj._changed.append(self.name)

class IntDescriptor(StringDescriptor):
  TYPE = int

class BooleanDescriptor(StringDescriptor):
  TYPE = bool

class RemoteDescriptor:
  def __init__(self, path, params, object_type):
    self._value = None
    self._path = path
    self._params = params
    self._object_type = object_type

  def __get__(self, obj, objType=None):
    if self._value is None:
      params = {}
      for key in self._params.keys():
        params[key] = self._params[key] % obj._properties
      self._value = obj._get(self._path, params, self._object_type)
    return self._value



