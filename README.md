Tadaist is a straight-forward wrapper around the Todoist API.

It's meant to be simple to use, and work in a logical manner.

```python
>>> from tadaist import Tadaist
>>> t = Tadaist(<YOUR API KEY>)
>>> inbox = t.inbox # Retrieve the Inbox
>>> inbox
Project: Inbox
>>> inbox.tasks # Tasks as simple enough to grab from a project
[Task: Do something, Task: Do something else]
>>> work = t.project('Work') # Grab a specifc project by name, or by ID
>>> work
Project: Work
>>> work.id
123456789
>>> task = work.new_task() # This creates an empty task that will be added to this project
>>> task.content = "Hi I'm a test"
>>> task.id # No ID, or order (or anything) because it has not been saved.
>>> task.order
>>> task.save() # This adds it to Todoist
>>> task.id # And now the ID, order, and other parameters are present
987654321
>>> task.order
3
```

This is essentially a rough draft, with a lot of work left to do before it's usable.

For example, when you add a task to a project, it's saved, and you'll see it in Todoist app, but it won't show up in `project.tasks` becaues we cache the tasks. Obviously that needs to be fixed.

For now, though, you can:

- View, list, update, and create projects
- View, list, update, and create sections
- Create, list, update, and complete tasks
